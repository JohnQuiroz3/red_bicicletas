var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'id: ' + this.id + ' | color: ' + this.color + ' | modelo: ' + this.modelo +
        ' | ubicacion: ' + this.ubicacion;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(bici, cb){
    this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function(codigo, cb){
    return this.findOne({_id: codigo}, cb);
};

bicicletaSchema.statics.removeByCode = function(codigo, cb){
    return this.deleteOne({code: codigo}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);
/* 
var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + ' | color: ' + this.color + ' | modelo: ' + this.modelo +
        ' | ubicacion: ' + this.ubicacion;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(bici){
    Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = function(id){
    var bici = Bicicleta.allBicis.find(x => x.id == id);
    if(bici){
        return bici;
    }
    else{
        throw new Error(`No existe una bicicleta con el ID ${id}`)
    }
}

Bicicleta.removeById = function(id){
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == id){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'verde', 'urbana', [6.217563, -75.606742]);
var b = new Bicicleta(2, 'blanca', 'urbana', [6.233930, -75.587518]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta; */