var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const Token = require('./Token');
const mailer = require('../mailer/mailer');
const saltRounds = 10;

const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario.'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    reserva.save(cb);
};

usuarioSchema.methods.enviarEmailBienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const emailDestination = this.email;
    token.save(function(err){
        if(err){console.log(err.message);}
        const mailOptions = {
            from: process.env.from_email,
            to: emailDestination,
            subject: 'Verificacion de cuenta',
            html: 'Hola \n\n' + 'Para verificar su cuenta haga click en este enlace \n ' + process.env.HOST + '\/token/confirmation\/' + token.token + '.\n'
        };
        mailer.sendMail(mailOptions, function(err){
            if(err){console.log(err.message);}
            console.log('Se envio la verificación de la cuenta a ' + emailDestination);
        });
    });
};

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const emailDestination = this.email;
    token.save(function(err){
        if(err){return cb(err);}
        const mailOptions = {
            from: 'no-repy@redbicicletas.com',
            to: emailDestination,
            subject: 'Reseteo de password de cuenta',
            text: 'Hola \n\n' + 'Para resetear el password de su cuenta haga click en este enlace \n ' + 'http://localhost:3000'+ '\/resetPassword\/' + token.token + '.\n'
        };
        mailer.sendMail(mailOptions, function(err){
            if(err){return cb(err);}
            console.log('Se envio la verificación de la cuenta a ' + emailDestination);
        });
        return null;
    });
};

usuarioSchema.statics.findOrCreateByGoogle = function findOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if(result){
                callback(err, result);
            }else{
                console.log('-------------Condition----------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'Sin Nombre';
                values.verificado = true;
                values.password = condition.id;
                console.log('-------------Values----------------');
                console.log(values);
                self.create(values, function(err, result){
                    if(err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.statics.findOrCreateByFacebook = function findOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if(result){
                callback(err, result);
            }else{
                console.log('-------------Condition----------------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'Sin Nombre';
                values.verificado = true;
                values.password = condition.id;
                console.log('-------------Values----------------');
                console.log(values);
                self.create(values, function(err, result){
                    if(err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

module.exports = mongoose.model('Usuario', usuarioSchema);