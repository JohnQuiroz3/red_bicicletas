var Usuario = require('../models/usuario');
var Token = require('../models/Token');

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({token: req.params.token}, function(err, token){
            if(!token)return res.status(400).send({type: 'not-verified', msg: 'No se encontró el usuario. Es posible que haya expirado y deba solicitarlo nuevamente'});
            Usuario.findById(token._userId, function(err, usuario){
                if(!usuario)return res.status(400).send({msg: 'Usuario no encontrado'});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function(err){
                    if(!usuario)return res.status(500).send({msg: err.message});
                    res.redirect('/');
                });
            });
        });
    }
};