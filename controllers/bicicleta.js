var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, (err, bicicletas) => {
        res.render('bicicletas/index', {bicis: bicicletas});
    });
    // res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    var bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req, res){
    Bicicleta.findById(req.params.id, (err, bici) => {
        if (err) console.log(err);
        res.render('bicicletas/update', {bici: bici});
    });
/*     var bici = Bicicleta.findByCode(req.params.id);
    res.render('bicicletas/update', {bici}); */
}

exports.bicicleta_update_post = function(req, res){
    var values = {code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]};
    Bicicleta.findByIdAndUpdate(req.params.id, values, (err, bici) => {
        if(err){
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bici: bici});
        }else{
            res.redirect('/bicicletas');
            return;
        }
    });
/*     var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas'); */
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.findByIdAndDelete(req.body.id, (err) =>{
        if(err){
            next(err);
        }else{
            res.redirect('/bicicletas');
        }
    });
/*     Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas'); */
}