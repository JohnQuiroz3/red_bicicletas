var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('Connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            mongoose.disconnect();
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [6.217563, -75.606742]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(6.217563);
            expect(bici.ubicacion[1]).toEqual(-75.606742);
            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega una bici', (done) => {
            var a = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(a, function(err, nueva){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(a.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Encuentra la bicicleta con codigo 1', (done) => {
            var a = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(a, function(err, nueva){
                if(err) console.log(err);
                Bicicleta.findByCode(1, function(err, bici){
                    expect(bici.code).toBe(a.code);
                    expect(bici.color).toBe(a.color);
                    expect(bici.modelo).toBe(a.modelo);
                    done();
                });
            });
        });
    });
});

/* 
describe('Bicicleta.allBicis', () => {
    it('Comienza con dos', () => {
        expect(Bicicleta.allBicis.length).toBe(2);
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        var i = Bicicleta.allBicis.length;
        var a = new Bicicleta(3, 'verde', 'urbana', [6.217563, -75.606742]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(i + 1);
        expect(Bicicleta.allBicis[i]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Devuelve la bici con determinada id', () => {
        var a = new Bicicleta(4, 'negra', 'montaña', [6.217563, -75.606742]);
        Bicicleta.add(a);
        var target = Bicicleta.findById(a.id);
        expect(target).toBe(a);
    });
});

describe('Bicicleta.removeById', () => {
    it('Elimina una', () => {
        var i = Bicicleta.allBicis.length;
        Bicicleta.removeById(Bicicleta.allBicis[0].id);
        expect(Bicicleta.allBicis.length).toBe(i - 1);
    });
}); */