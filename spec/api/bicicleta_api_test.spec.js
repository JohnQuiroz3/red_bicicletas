var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        const db = mongoose.createConnection(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
        mongoose.connection = db;
        
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('Connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            mongoose.connection.close;
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var header = {'content-type' : 'application/json'};
            var a = `{"code": 10, "color": "azul", "modelo": "pista", "lat": 6.217563, "lng": -75.606742}`;
            request.post({
                headers: header,
                url: base_url + '/create',
                body: a
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.ubicacion[0]).toBe(6.217563);
                expect(bici.ubicacion[1]).toBe(-75.606742);
                done();
            })
        });
    });
/* 
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var header = {'content-type' : 'application/json'};
            var i = Bicicleta.allBicis.length + 1;
            var a = `{"id": ${i}, "color": "azul", "modelo": "pista", "lat": 6.217563, "lng": -75.606742}`;
            request.post({
                headers: header,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: a
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(i).color).toBe("azul");
                done();
            })
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var header = {'content-type' : 'application/json'};
            var a = `{"id": 1, "color": "morado", "modelo": "pista", "lat": 6.217563, "lng": -75.606742}`;
            request.post({
                headers: header,
                url: 'http://localhost:3000/api/bicicletas/1/update',
                body: a
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("morado");
                done();
            })
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var header = {'content-type' : 'application/json'};
            var bici = Bicicleta.allBicis[0];
            var i = bici.id;
            var a = `{"id": ${i}}`;
            request.delete({
                headers: header,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: a
            }, function(error, response, body){
                expect(response.statusCode).toBe(204);
                done();
            })
        });
    }); */
});